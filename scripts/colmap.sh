#! /bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: <input_dir>"
  exit 1
fi

input_dir="$1"
if [ ! -d "$input_dir" ]; then
  echo "Input directory not found!"
  exit 2
fi

cd "$input_dir"

if [ ! -f ../bumblebee.json ]; then
  echo "Expected ../bumblebee.json not found!"
  exit 3
fi

colmap database_creator \
--database_path database.db

colmap feature_extractor \
--database_path database.db \
--image_path cam0 \
--ImageReader.camera_model OPENCV_FISHEYE \
--ImageReader.single_camera 1 \
--ImageReader.camera_params 545.7402000594014,546.4624873938807,516.7898455908171,399.68834148863493,-0.06983837053126551,0.030679193251357234,-0.029318268716673087,0.008383563478792275

colmap feature_extractor \
--database_path database.db \
--image_path cam1 \
--ImageReader.camera_model OPENCV_FISHEYE \
--ImageReader.single_camera 1 \
--ImageReader.camera_params 543.6000821015224,544.4238023037506,524.280213672173,381.4178892040695,-0.059680378064520846,0.01009738651194745,-0.01025701371639368,0.002248862281197446

colmap exhaustive_matcher \
--database_path database.db

mv cam1/* cam0
rm -r cam1
mv cam0 images

mkdir reconstruction

colmap mapper \
--database_path database.db \
--image_path images \
--output_path reconstruction \
--Mapper.ba_refine_focal_length 0 \
--Mapper.ba_refine_principal_point 0 \
--Mapper.ba_refine_extra_params 0 \
--Mapper.ba_global_use_pba 0

mkdir reconstruction/1

colmap rig_bundle_adjuster \
--input_path reconstruction/0 \
--output_path reconstruction/1 \
--rig_config_path ../bumblebee.json \
--BundleAdjustment.refine_focal_length 0 \
--BundleAdjustment.refine_principal_point 0 \
--BundleAdjustment.refine_extra_params 0 \
--BundleAdjustment.refine_extrinsics 1 \
--refine_relative_poses 0

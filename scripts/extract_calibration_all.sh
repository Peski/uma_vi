#! /bin/bash

bumblebee="calib_cam-bumblebeeCHANGE_2019-02-07-17-16-40"
for calib_seq in $bumblebee
do
  bash /home/david/workspace/uma_vi/scripts/extract_calibration.sh /home/david/workspace/temp/uma_vi/calib_seqs/cam/$calib_seq.bag /home/david/workspace/mount/UMA_VI/calibration/sequences "cam0 cam1"
  bash /home/david/workspace/uma_vi/bin/exposure --bumblebee /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam0/data.csv
  bash /home/david/workspace/uma_vi/bin/exposure --bumblebee /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam1/data.csv
  rm -R /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam2
  rm -R /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam3
  rm -R /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/imu0
done

ueye="calib_cam-ueyeMEDIUM_2019-02-07-16-39-35 calib_cam-ueyeFAR_2019-02-07-16-54-49 calib_cam-ueyeOUTDOOR_2019-02-05-18-07-13"
for calib_seq in $ueye
do
  bash /home/david/workspace/uma_vi/scripts/extract_calibration.sh /home/david/workspace/temp/uma_vi/calib_seqs/cam/$calib_seq.bag /home/david/workspace/mount/UMA_VI/calibration/sequences "cam2 cam3"
  bash /home/david/workspace/uma_vi/bin/exposure --ueye /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam2/data.csv
  bash /home/david/workspace/uma_vi/bin/exposure --ueye /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam3/data.csv
  rm -R /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam0
  rm -R /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam1
  rm -R /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/imu0
done

bumblebee_imu="calib_imu-bumblebeeENG2_2019-02-14-17-43-58"
for calib_seq in $bumblebee_imu
do
  bash /home/david/workspace/uma_vi/scripts/extract_calibration.sh /home/david/workspace/temp/uma_vi/calib_seqs/imu_cam/$calib_seq.bag /home/david/workspace/mount/UMA_VI/calibration/sequences "imu0 cam0 cam1"
  bash /home/david/workspace/uma_vi/bin/exposure --bumblebee /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam0/data.csv
  bash /home/david/workspace/uma_vi/bin/exposure --bumblebee /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam1/data.csv
  rm -R /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam2
  rm -R /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam3
done

ueye_imu="calib_imu-ueyeENG2_2019-02-14-17-53-32"
for calib_seq in $ueye_imu
do
  bash /home/david/workspace/uma_vi/scripts/extract_calibration.sh /home/david/workspace/temp/uma_vi/calib_seqs/imu_cam/$calib_seq.bag /home/david/workspace/mount/UMA_VI/calibration/sequences "imu0 cam2 cam3"
  bash /home/david/workspace/uma_vi/bin/exposure --ueye /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam2/data.csv
  bash /home/david/workspace/uma_vi/bin/exposure --ueye /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam3/data.csv
  rm -R /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam0
  rm -R /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq/cam1
done

imu_cam="calib_imu-camBRIDGE_2019-02-01-17-25-10 calib_imu-camOUTDOOR_2019-02-01-17-38-34"
for calib_seq in $imu_cam
do
  bash /home/david/workspace/uma_vi/scripts/extract.sh /home/david/workspace/temp/uma_vi/calib_seqs/imu_cam/$calib_seq.bag /home/david/workspace/mount/UMA_VI/calibration/sequences
  bash /home/david/workspace/uma_vi/scripts/exposure.sh /home/david/workspace/mount/UMA_VI/calibration/sequences/$calib_seq
done
#! /bin/bash

if [ "$#" -ne 2 ]; then
  echo "Usage: <sequences_path> <loop_closures_path>"
  exit 1
fi

seq_path="$1"
if [ ! -d "$seq_path" ]; then
  echo "Sequences path not found!"
  exit 2
fi
seq_path=${seq_path%/}

lcs_path="$2"
if [ ! -d "$lcs_path" ]; then
  echo "Loop closures path not found!"
  exit 3
fi
lcs_path=${lcs_path%/}

for i in $seq_path/seq*
do
    name="${i%/}"
    name="${name##*/}"

    bash /home/david/workspace/uma_vi/scripts/extract_trajectory.sh "$lcs_path/$name/reconstruction/1" "$seq_path/$name/imu0_trajectory.txt"
done

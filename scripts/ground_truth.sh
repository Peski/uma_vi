#! /bin/bash

# Export output_path

if [ "$#" -ne 3 ]; then
  echo "Usage: <input_dir> <sn> <eo>"
  exit 1
fi

input_dir="$1"
if [ ! -d "$input_dir" ]; then
  echo "Input file not found!"
  exit 2
fi

input_dir="${input_dir%/}"
output_dir="${input_dir##*/}"

/home/david/workspace/uma_vi/bin/split --cameras=cam0,cam1 --sn="$2" --eo="$3" --output_path="$output_dir" "$input_dir"

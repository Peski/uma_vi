#! /bin/bash

if [ "$#" -lt 2 ]; then
  echo "Usage: <output_path> <input_file1> [input_file2] ..."
  exit 1
fi

output_dir_base="$1"
if [ "${output_dir_base: -1}" != "/" ]; then
  output_dir_base="$output_dir_base/"
fi

if [ ! -d "$output_dir_base" ]; then
  echo "Invalid output directory!"
  exit 2
fi

for i in $(seq 2 "$#")
do
  input_file="${!i}"
  if [ ! -f "$input_file" ]; then
    echo "Input file not found!"
    continue
  fi

  filename="${input_file##*/}"
  filename="${filename%.*}"

  output_dir="$output_dir_base$filename"

  /home/david/workspace/uma_vi/bin/extract --output_path="$output_dir" --compression_level=9 "$input_file"

  bash /home/david/workspace/uma_vi/scripts/exposure.sh "$output_dir"

  python /home/david/workspace/uma_vi/scripts/rosbagCreator.py "$output_dir"
done


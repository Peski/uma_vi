#! /bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: <input_dir>"
  exit 1
fi

input_dir="$1"
if [ ! -d "$input_dir" ]; then
  echo "Input directory not found!"
  exit 2
fi

input_dir="${input_dir%/}"

for i in $input_dir/*.zip
do
  zip -T "$i";
done
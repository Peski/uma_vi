#! /bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: <input_dir>"
  exit 1
fi

input_dir="$1"
if [ ! -d "$input_dir" ]; then
  echo "Input directory not found!"
  exit 2
fi

input_dir="${input_dir%/}"

/home/david/workspace/uma_vi/bin/exposure --bumblebee "$input_dir/cam0/data.csv"
/home/david/workspace/uma_vi/bin/exposure --bumblebee "$input_dir/cam1/data.csv"
/home/david/workspace/uma_vi/bin/exposure --ueye "$input_dir/cam2/data.csv"
/home/david/workspace/uma_vi/bin/exposure --ueye "$input_dir/cam3/data.csv"

#! /bin/bash

qvec="0.00409481421498524,0.00534718495611909,-0.00997782301212552,0.999927538953463"
tvec="0.0486448,0.11620352,0.0000557"

if [ "$#" -ne 2 ]; then
  echo "Usage: <input_path> <output_file>"
  exit 1
fi

input_file="$1"
if [ ! -d "$input_file" ]; then
  echo "Input path not found!"
  exit 2
fi

/home/david/workspace/uma_vi/bin/extract_trajectory --qvec=$qvec --tvec=$tvec "$1" "$2"
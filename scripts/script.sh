#! /bin/bash

./bin/extract --output_path=/home/david/workspace/data/uma_vi/calibration/calib_cam --sensors=cam0,cam1 --s=3 /home/david/workspace/mount/uma_vi/calib_seqs/calib_cam-ueyeCLOSE_2019-02-07-16-34-54.bag
./bin/extract --output_path=/home/david/workspace/data/uma_vi/calibration/calib_cam --sensors=cam2,cam3 --s=7 /home/david/workspace/mount/uma_vi/calib_seqs/calib_cam-ueyeCLOSE_2019-02-07-16-34-54.bag

./bin/extract --output_path=/home/david/workspace/data/uma_vi/calibration/calib_cam_med --sensors=cam0,cam1 --s=3 /home/david/workspace/mount/uma_vi/calib_seqs/calib_cam-ueyeMEDIUM_2019-02-07-16-39-35.bag
./bin/extract --output_path=/home/david/workspace/data/uma_vi/calibration/calib_cam_med --sensors=cam2,cam3 --s=7 /home/david/workspace/mount/uma_vi/calib_seqs/calib_cam-ueyeMEDIUM_2019-02-07-16-39-35.bag

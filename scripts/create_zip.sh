#! /bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: <input_dir>"
  exit 1
fi

input_dir="$1"
if [ ! -d "$input_dir" ]; then
  echo "Input directory not found!"
  exit 2
fi

input_dir="${input_dir%/}"

for f in $input_dir/*
do
  if [ -d "$f" ] && [ ! -e "$f.zip" ]; then
    echo "$f"
    zip -r -q "$f.zip" "$f"
    md5sum "$f.zip" >> checksum.md5
  fi
done
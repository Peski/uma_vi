#! /bin/bash

if [ "$#" -ne 2 ]; then
  echo "Usage: <input_file> <output_path>"
  exit 1
fi

input_file="$1"
if [ ! -f "$input_file" ]; then
  echo "Input file not found!"
  exit 2
fi

output_dir="$2"
if [ "${output_dir: -1}" != "/" ]; then
  output_dir="$output_dir/"
fi

filename="${input_file##*/}"
filename="${filename%.*}"

output_dir="$output_dir$filename"

/home/david/workspace/uma_vi/bin/extract --output_path="$output_dir" --compression_level=9 "$1"
#! /bin/bash

cd /home/david/workspace/ruben/datasets/UMA_VI/loop_closure/

while read line;
do
  bash /home/david/workspace/uma_vi/scripts/ground_truth.sh $line
done < image_segments.txt

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <iostream>
#include <iterator>
#include <limits>
#include <string>
#include <utility>
#include <vector>

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "io.hpp"
#include "macros.h"
#include "sequence.hpp"

static const std::string IMU_HEADER = "#timestamp [ns],w_x [rad s^-1],w_y [rad s^-1],w_z [rad s^-1],a_x [m s^-2],a_y [m s^-2],a_z [m s^-2]";
static const std::string CAM_HEADER = "#timestamp [ns],filename";
static const std::string EXPOSURE_HEADER = "#timestamp [ns],filename,exposure";

struct sensor_info {
  std::string name;
  std::string topic;
  std::string header;
  io::timestamp_t period;
};

io::timestamp_t get_origin(rosbag::Bag& bag) {
    rosbag::View view(bag);
    return io::timestamp(view.begin()->getTime());
}

static inline void process_imu(const boost::filesystem::path& output_path, rosbag::View& imu_view, const sensor_info& info, const io::timestamp_t& origin) {
  boost::filesystem::path imu_dir = output_path / info.name;

  using data_t = std::uint32_t;
  std::vector<data_t> imu_timestamps;
  imu_timestamps.reserve(imu_view.size());

  for (const rosbag::MessageInstance& m : imu_view) {
    sensor_msgs::Imu::ConstPtr msg = m.instantiate<sensor_msgs::Imu>();
    RUNTIME_ASSERT(msg != nullptr);
    //imu_timestamps.emplace_back(io::timestamp(msg->header.stamp));
    imu_timestamps.emplace_back(msg->header.seq);
  }
  std::sort(imu_timestamps.begin(), imu_timestamps.end());

  std::vector<data_t> imu_missing;
  if (!imu_timestamps.empty()) {
    data_t seq_start = imu_timestamps.front();
    data_t seq_end = imu_timestamps.back();

    for (data_t i = seq_start; i <= seq_end; ++i) {
      if (!std::binary_search(imu_timestamps.cbegin(), imu_timestamps.cend(), i))
        imu_missing.push_back(i);
    }

    if (!imu_missing.empty()) {
      std::cout << imu_missing.size() << " imu measurements missing" << std::endl;
      imu_timestamps.insert(imu_timestamps.cend(), imu_missing.cbegin(), imu_missing.cend());
      std::sort(imu_timestamps.begin(), imu_timestamps.end());
    }
  }

  //imu_timestamps = downsample(imu_timestamps, FLAGS_o, FLAGS_n, FLAGS_s);
  //io::timestamp_t downsampled_period = info.period*(FLAGS_s + 1);
  io::timestamp_t downsampled_period = info.period;

  std::size_t ctr = 1;
  std::size_t n = imu_view.size();

  std::vector<io::imu_data_t> imu_records;
  for(const rosbag::MessageInstance& m: imu_view) {
    std::cout << "\r" << ctr++ << " / " << n << std::flush;

    sensor_msgs::Imu::ConstPtr imu_msg = m.instantiate<sensor_msgs::Imu>();
    RUNTIME_ASSERT(imu_msg != nullptr);

    //io::timestamp_t ts = io::timestamp(imu_msg->header.stamp);
    data_t id = imu_msg->header.seq;
    std::vector<data_t>::const_iterator cit = std::lower_bound(imu_timestamps.cbegin(), imu_timestamps.cend(), id);
    //RUNTIME_ASSERT(cit != imu_timestamps.cend());
    if (cit == imu_timestamps.cend()) continue;
    if (*cit != id) continue;
    std::size_t idx = std::distance(imu_timestamps.cbegin(), cit);
    io::timestamp_t ns = origin + downsampled_period*idx; // reconstruct timestamp

    geometry_msgs::Vector3 w = imu_msg->angular_velocity;
    geometry_msgs::Vector3 a = imu_msg->linear_acceleration;
            
    imu_records.emplace_back(io::imu_data_t{ns, w.x, w.y, w.z, a.x, a.y, a.z});
  }

  std::sort(imu_records.begin(), imu_records.end());

  bool imu_saved = io::write_file<io::imu_data_t>(imu_records, (imu_dir / "data.csv").string(), info.header);
  RUNTIME_ASSERT(imu_saved);
}

static inline void process_images(const boost::filesystem::path& output_path, const std::vector<int>& compression_params, rosbag::View& cam_view, const sensor_info& info, const io::timestamp_t& origin) {
  boost::filesystem::path cam_dir = output_path / info.name;
  boost::filesystem::create_directory(boost::filesystem::path(cam_dir) / "data");

  std::vector<io::timestamp_t> cam_timestamps;
  cam_timestamps.reserve(cam_view.size());

  for (const rosbag::MessageInstance& m : cam_view) {
    sensor_msgs::Image::ConstPtr cam_msg = m.instantiate<sensor_msgs::Image>();
    RUNTIME_ASSERT(cam_msg != nullptr);
    cam_timestamps.emplace_back(io::timestamp(cam_msg->header.stamp));
  }
  std::sort(cam_timestamps.begin(), cam_timestamps.end());
  //cam_timestamps = downsample(cam_timestamps, FLAGS_o, FLAGS_n, FLAGS_s);
  //io::timestamp_t downsampled_period = info.period*(FLAGS_s + 1);
  io::timestamp_t downsampled_period = info.period;

  std::size_t ctr = 1;
  std::size_t n = cam_view.size();

  //io::Records cam_records;
  std::vector<io::exposure_t> cam_exposures;
  for(const rosbag::MessageInstance& m : cam_view) {
    std::cout << "\r" << ctr++ << " / " << n << " " << std::flush;

    sensor_msgs::Image::ConstPtr cam_msg = m.instantiate<sensor_msgs::Image>();
    RUNTIME_ASSERT(cam_msg != nullptr);

    io::timestamp_t ts = io::timestamp(cam_msg->header.stamp);
    std::vector<io::timestamp_t>::const_iterator cit = std::lower_bound(cam_timestamps.cbegin(), cam_timestamps.cend(), ts);
    //RUNTIME_ASSERT(cit != cam_timestamps.cend());
    if (cit == cam_timestamps.cend()) continue;
    if (*cit != ts) continue;
    std::size_t idx = std::distance(cam_timestamps.cbegin(), cit);
    io::timestamp_t ns = origin + downsampled_period*idx; // + FLAGS_cam_shift; // reconstruct timestamp

    cv::Mat image;
    if (cam_msg->encoding.compare("bayer_grbg8") == 0) {
        // Debayer
        const cv::Mat bayer(cam_msg->height, cam_msg->width, CV_8UC1, const_cast<uint8_t*>(&cam_msg->data[0]), cam_msg->step);

        image = cv::Mat(cam_msg->height, cam_msg->width, CV_8UC3);
        cv::cvtColor(bayer, image, cv::COLOR_BayerGB2BGR_VNG);
    } else if (cam_msg->encoding.compare("bgr8") == 0) {
        image = cv::Mat(cam_msg->height, cam_msg->width, CV_8UC3, const_cast<uint8_t*>(&cam_msg->data[0]), cam_msg->step);
    } else if (cam_msg->encoding.compare("mono8") == 0) {
        image = cv::Mat(cam_msg->height, cam_msg->width, CV_8UC1, const_cast<uint8_t*>(&cam_msg->data[0]), cam_msg->step);
    } else {
        throw std::runtime_error("Unsupported image encoding! (at " __AT__ ")");
    }

    io::timestamp_t exposure_time = std::stoul(cam_msg->header.frame_id);

    std::string image_name = std::to_string(ns) + ".png";
    boost::filesystem::path image_path = cam_dir / "data" / image_name;
    cv::imwrite(image_path.string(), image, compression_params);

    //cam_records.emplace_back(std::make_pair(ns, image_name));
    cam_exposures.emplace_back(io::exposure_t(ns, image_name, exposure_time));
  }

//  std::sort(cam_records.begin(), cam_records.end());
    using namespace io;
    std::sort(cam_exposures.begin(), cam_exposures.end());

//  bool cam_saved = io::write_file(cam_records, (cam_dir / "data.csv").string(), info.header);
//  RUNTIME_ASSERT(cam_saved);

  bool exposure_saved = io::write_file(cam_exposures, (cam_dir / "data.csv").string(), EXPOSURE_HEADER);
  RUNTIME_ASSERT(exposure_saved);
}


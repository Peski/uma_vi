
#define PROGRAM_NAME \
    "extract"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(string, output_path, "", "Output images path")                                       \
    FLAG_CASE(uint64, compression_level, 3, "PNG compression level (0-9)")                         \
    FLAG_CASE(string, sensors, "", "Extract only desired sensors")                                 \
    FLAG_CASE(uint64, o, 0, "Sequence offset")                                                     \
    FLAG_CASE(uint64, n, 0, "Max sequence length")                                                 \
    FLAG_CASE(uint64, s, 0, "Skip sequence elements")

#define ARGS_CASES                                                                                 \
    ARG_CASE(rosbag)

#include <algorithm>
#include <iostream>
#include <iterator>
#include <limits>
#include <stdexcept>
#include <string>
#include <vector>

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>

#include <boost/filesystem.hpp>

#include "args.hpp"
#include "io.hpp"
#include "macros.h"
#include "misc.h"

// TODO config yaml?

#include "process.hpp"

std::vector<sensor_info> sensors_info = {{"imu0", "/imu/data", IMU_HEADER, 4000000ull},
  {"cam0", "/camera/left/image_raw", CAM_HEADER, 80000000ull}, {"cam1", "/camera/right/image_raw", CAM_HEADER, 80000000ull},
  {"cam2", "/ueye/cam1/image_raw", CAM_HEADER, 40000000ull}, {"cam3", "/ueye/cam2/image_raw", CAM_HEADER, 40000000ull}};

void ValidateArgs() {
    RUNTIME_ASSERT(boost::filesystem::is_regular_file(ARGS_rosbag));
}

void ValidateFlags() {
    if (FLAGS_output_path.empty()) {
        FLAGS_output_path = boost::filesystem::current_path().string();
    } else {
        boost::filesystem::create_directories(FLAGS_output_path);
        RUNTIME_ASSERT(boost::filesystem::is_directory(FLAGS_output_path));
    }

    //boost::filesystem::create_directory(boost::filesystem::path(FLAGS_output_path) / IMU0_DIR);
    //RUNTIME_ASSERT(boost::filesystem::is_empty(boost::filesystem::path(FLAGS_output_path) / IMU0_DIR));

    //boost::filesystem::create_directory(boost::filesystem::path(FLAGS_output_path) / CAM0_DIR);
    //RUNTIME_ASSERT(boost::filesystem::is_empty(boost::filesystem::path(FLAGS_output_path) / CAM0_DIR));

    //boost::filesystem::create_directory(boost::filesystem::path(FLAGS_output_path) / CAM1_DIR);
    //RUNTIME_ASSERT(boost::filesystem::is_empty(boost::filesystem::path(FLAGS_output_path) / CAM1_DIR));

    RUNTIME_ASSERT(FLAGS_compression_level < 10);

    if (FLAGS_sensors.empty()) {
        std::vector<std::string> sensors;
        for (const sensor_info& sensor_info : sensors_info) {
            sensors.push_back(sensor_info.name);
        }
        FLAGS_sensors = VectorToCSV(sensors);
    }
}

void ValidateSensorInfo(const std::vector<sensor_info>& sensors_info) {
    for (const sensor_info& sensor_info : sensors_info) {
        if (FLAGS_sensors.find(sensor_info.name) == std::string::npos) continue;
        boost::filesystem::create_directory(boost::filesystem::path(FLAGS_output_path) / sensor_info.name);
        RUNTIME_ASSERT(boost::filesystem::is_empty(boost::filesystem::path(FLAGS_output_path) / sensor_info.name));
    }
}

int main(int argc, char* argv[]) {

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateArgs();
    ValidateFlags();

    ValidateSensorInfo(sensors_info);

    // Set input config
    boost::filesystem::path output_path(FLAGS_output_path);
    std::vector<int> compression_params = {CV_IMWRITE_PNG_COMPRESSION, static_cast<int>(FLAGS_compression_level)};

    std::cout << "Loading " << ARGS_rosbag << " ..." << std::endl;

    rosbag::Bag bag;
    bag.open(ARGS_rosbag, rosbag::bagmode::Read);

    io::timestamp_t origin = get_origin(bag);

    for (const sensor_info& sensor_info : sensors_info) {
        if (FLAGS_sensors.find(sensor_info.name) == std::string::npos) continue;

        std::cout << "Processing " << sensor_info.topic << " ..." << std::endl;

        std::string name_prefix = sensor_info.name.substr(0, 3);
        rosbag::View view(bag, rosbag::TopicQuery(sensor_info.topic));
        if (name_prefix.compare("imu") == 0) {
            process_imu(output_path, view, sensor_info, origin);
        } else if (name_prefix.compare("cam") == 0) {
            process_images(output_path, compression_params, view, sensor_info, origin);
        } else {
            throw std::runtime_error("Unsupported sensor type! (at " __AT__ ")");
        }

        std::cout << std::endl;
    }

    bag.close();

    return 0;
}

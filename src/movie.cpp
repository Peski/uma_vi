
#define PROGRAM_NAME \
    "movie"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(double, fps, 30.0, "Frame rate (1-60)")                                              \
    FLAG_CASE(string, size, "", "Frame size")                                                      \
    FLAG_CASE(uint64, o, 0, "Sequence offset")                                                     \
    FLAG_CASE(uint64, n, 0, "Max sequence length")                                                 \
    FLAG_CASE(uint64, s, 0, "Skip sequence elements")

#define ARGS_CASES                                                                                 \
    ARG_CASE(input_path)                                                                           \
    ARG_CASE(output_file)

#include <cstdint>
#include <iostream>
#include <string>
#include <vector>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <boost/filesystem.hpp>

#include "args.hpp"
#include "io.hpp"
#include "macros.h"
#include "misc.h"
#include "sequence.hpp"

void ValidateArgs() {
    RUNTIME_ASSERT(boost::filesystem::is_directory(ARGS_input_path));
    RUNTIME_ASSERT(!boost::filesystem::exists(ARGS_output_file));
}

void ValidateFlags() {
    RUNTIME_ASSERT(1.0 <= FLAGS_fps  && FLAGS_fps <= 60.0);

    if (!FLAGS_size.empty()) {
        std::vector<int> size = CSVToVector<int>(FLAGS_size);
        RUNTIME_ASSERT(size.size() == 2);
        RUNTIME_ASSERT(size[0] > 0 && size[1] > 0);
    }
}

int main (int argc, char **argv) {

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateArgs();
    ValidateFlags();

    // Load image sequnce
    std::vector<std::string> sequence = getSequence<io::timestamp_t>(ARGS_input_path, FLAGS_o, FLAGS_n, FLAGS_s);

    // Determine frame size
    cv::Size frame_size(0, 0);
    if (FLAGS_size.empty()) { // Auto
        cv::Mat img = imread(sequence.front(), cv::IMREAD_COLOR);
        frame_size = img.size();
    } else {
        std::vector<int> size = CSVToVector<int>(FLAGS_size);
        frame_size.width = size[0];
        frame_size.height = size[1];
    }

    cv::VideoWriter outputVideo(ARGS_output_file, CV_FOURCC('M', 'J', 'P', 'G'), FLAGS_fps, frame_size);
    RUNTIME_ASSERT(outputVideo.isOpened());

    std::size_t ctr = 1;
    std::size_t n = sequence.size();
    for (const std::string& file_name : sequence) {
        std::cout << "\r" << ctr++ << " / " << n << " " << std::flush;
        cv::Mat img = imread(file_name, cv::IMREAD_COLOR);
        if (img.size() != frame_size) cv::resize(img, img, frame_size);
        outputVideo.write(img);
    }
    std::cout << std::endl;

    outputVideo.release();

    return 0;
}

#define PROGRAM_NAME \
    "extract_trajectory"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(string, ref_cam, "cam0", "Reference camera prefix")                                  \
    FLAG_CASE(string, qvec, "1.0,0.0,0.0,0.0", "Quaternion rectification [w,x,y,z]")               \
    FLAG_CASE(string, tvec, "0.0,0.0,0.0", "Translation rectification [x,y,z]")

#define ARGS_CASES                                                                                 \
    ARG_CASE(input_path)                                                                           \
    ARG_CASE(output_file)

// STL
#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "colmap/pose.hpp"
#include "colmap/reconstruction.hpp"

#include "args.hpp"
#include "io.hpp"
#include "macros.h"
#include "misc.h"
#include "sequence.hpp"

static const std::string TRAJ_HEADER = "#timestamp [ns],tx,ty,tz,qw,qx,qy,qz";

void ValidateArgs() {
    RUNTIME_ASSERT(boost::filesystem::is_directory(ARGS_input_path));
    RUNTIME_ASSERT(!boost::filesystem::exists(ARGS_output_file));
}

void ValidateFlags() {
    std::vector<double> qvec = CSVToVector<double>(FLAGS_qvec);
    RUNTIME_ASSERT(qvec.size() == 4);

    std::vector<double> tvec = CSVToVector<double>(FLAGS_tvec);
    RUNTIME_ASSERT(tvec.size() == 3);
}

int main(int argc, char* argv[]) {

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateArgs();
    ValidateFlags();

    // Read model
    colmap::Reconstruction reconstruction;
    reconstruction.Read(ARGS_input_path);

/*
    PrintHeading1("Reconstruction Info");
    std::cout << "Number of cameras: " << reconstruction.Cameras().size() << std::endl;
    std::cout << "Number of images: " << reconstruction.Images().size() << std::endl;
    std::cout << "Number of 3d points: " << reconstruction.Points3D().size() << std::endl;
    std::cout << std::endl;
*/

    io::OUTPUT_SEPARATOR = ",";
    io::OUTPUT_PRECISION = 9;

    std::vector<double> qvec = CSVToVector<double>(FLAGS_qvec);
    Eigen::Map<Eigen::Vector4d> q(qvec.data());
    q = colmap::NormalizeQuaternion(q);

    std::vector<double> tvec = CSVToVector<double>(FLAGS_tvec);
    Eigen::Map<Eigen::Vector3d> t(tvec.data());

    Eigen::Isometry3d T_inc(Eigen::Quaterniond(q(0), q(1), q(2), q(3)));
    T_inc.translation() = t;

    io::Trajectory trajectory;
    for (const std::pair<colmap::image_t, colmap::Image>& image_entry : reconstruction.Images()) {
        const colmap::Image& image = image_entry.second;
        if (image.Name().find(FLAGS_ref_cam) != 0) continue;

        const io::timestamp_t id = sequence_id<io::timestamp_t>::get_sid(image.Name());

        Eigen::Vector4d qvec;
        Eigen::Vector3d tvec;
        colmap::InvertPose(image.Qvec(), image.Tvec(), &qvec, &tvec);

        Eigen::Isometry3d T(Eigen::Quaterniond(qvec(0), qvec(1), qvec(2), qvec(3)));
        T.translation() = tvec;

        io::Trajectory::value_type trajectory_entry;
        trajectory_entry.id = id;
        trajectory_entry.pose = T * T_inc;

        trajectory.emplace_back(trajectory_entry);
    }

    using namespace io;
    std::sort(trajectory.begin(), trajectory.end());
    RUNTIME_ASSERT(io::write_file<io::Trajectory::value_type>(trajectory, ARGS_output_file, TRAJ_HEADER));

    return 0;
}


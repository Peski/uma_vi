
#define PROGRAM_NAME \
    "check"

#define FLAGS_CASES

#define ARGS_CASES                                                                                 \
    ARG_CASE(rosbag)

#include <cstdint>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <boost/filesystem.hpp>

#include "args.hpp"
#include "macros.h"

#include "process.hpp"

std::vector<sensor_info> sensors_info = {{"cam0", "/camera/left/image_raw", CAM_HEADER, 80000000ull}, {"cam1", "/camera/right/image_raw", CAM_HEADER, 80000000ull},
  {"cam2", "/ueye/cam1/image_raw", CAM_HEADER, 40000000ull}, {"cam3", "/ueye/cam2/image_raw", CAM_HEADER, 40000000ull},
  {"imu0", "/imu/data", IMU_HEADER, 4000000ull}};

static const std::string OPENCV_WINDOW = "Image window";

void ValidateArgs() {
    RUNTIME_ASSERT(boost::filesystem::is_regular_file(ARGS_rosbag));
}

void ValidateFlags() {
}

int main(int argc, char* argv[]) {

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateArgs();
    ValidateFlags();

    std::cout << "Loading " << ARGS_rosbag << " ..." << std::endl;

    rosbag::Bag bag;
    bag.open(ARGS_rosbag, rosbag::bagmode::Read);

    std::unordered_map<std::string, std::size_t> messages;
    for (const sensor_info& sensor_info : sensors_info) {
        //std::cout << "Processing " << sensor_info.topic << " ..." << std::endl;

        //std::string name_prefix = sensor_info.name.substr(0, 3);
        rosbag::View view(bag, rosbag::TopicQuery(sensor_info.topic));
        messages[sensor_info.name] = view.size();
/*
        if (name_prefix.compare("imu") == 0) {
            process_imu(output_path, view, sensor_info, origin);
        } else if (name_prefix.compare("cam") == 0) {
            process_images(output_path, compression_params, view, sensor_info, origin);
        } else {
            throw std::runtime_error("Unsupported sensor type! (at " AT ")");
        }
*/

        //std::cout << std::endl;
    }

    std::cout << "Bumblebee: " << std::flush;
    if (messages.at("cam0") != messages.at("cam1")) {
        std::cout << "lost messages!" << std::endl;
        return -1;
    }
    std::size_t bumblebee = messages.at("cam0");
    std::cout << "OK!" << std::endl;

    std::cout << "uEye: " << std::flush;
    if (messages.at("cam2") != messages.at("cam3")) {
        std::cout << "lost messages!" << std::endl;
        return -1;
    }
    std::size_t ueye = messages.at("cam2");

    if ((2*(bumblebee-1)+1) != ueye) {
        std::cout << "lost messages!" << std::endl;
        return -1;
    }
    std::cout << "OK!" << std::endl;

    std::size_t imu = messages.at("imu0");
    std::cout << "IMU: " << std::flush;
    if ((10*(ueye-1)+1) != imu) {
        // TODO check if recovery is available
        std::cout << "lost messages!" << std::endl;
        return -1;
    }
    std::cout << "OK!" << std::endl;

    bag.close();

    return 0;
}
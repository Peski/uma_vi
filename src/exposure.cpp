
#define PROGRAM_NAME \
    "exposure"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(bool, bumblebee, false, "For Bumblebee2 cameras")                                    \
    FLAG_CASE(bool, ueye, false, "For uEye cameras")                                               \
    FLAG_CASE(int64, cam_shift, 0, "Timestamp offset correction")                                  \
    FLAG_CASE(bool, compensate_exposure, false, "Timestamp correction based on exposure time")

#define ARGS_CASES                                                                                 \
    ARG_CASE(input_file)

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <vector>

#include <boost/filesystem.hpp>

#include "args.hpp"
#include "io.hpp"
#include "macros.h"

static const std::string EXPOSURE_HEADER = "#timestamp [ns],filename,exposure [ns]";

io::timestamp_t absolute_value_bumblebee(std::uint16_t value) {
    double abs;
    io::timestamp_t abs_value;

    RUNTIME_ASSERT(8u <= value && value <= 210u);
    if (value <= 56u) {
        if (value == 56u) { // corner case
            abs = 0.5*(0.00780962216624685 + 0.0625002064344336)*value + 0.5*(-0.0323249370277077 + -3.09515029266304); // avg
        } else {
            abs = 0.00780962216624685*value + -0.0323249370277077; // to ms conversion
        }
    } else if (value <= 210u) {
        abs = 0.0625002064344336*value + -3.09515029266304; // to ms conversion
    }

    abs_value = std::round(abs*1000.0); // to integer us conversion
    abs_value *= io::timestamp_t(1000ul); // to integer ns

    return abs_value;
}

io::timestamp_t absolute_value_ueye(std::uint16_t value) {
    io::timestamp_t abs_value;

    abs_value = std::min(43300ul*static_cast<io::timestamp_t>(value) + 73850ul, 39909850ul); // to ns conversion

    return abs_value;
}

void ValidateArgs() {
    RUNTIME_ASSERT(boost::filesystem::is_regular_file(ARGS_input_file));
}

void ValidateFlags() {
    RUNTIME_ASSERT(FLAGS_bumblebee != FLAGS_ueye);
}

int main(int argc, char* argv[]) {

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateArgs();
    ValidateFlags();

    bool init = false;
    io::timestamp_t last_exposure;
    std::vector<io::exposure_t> exposure_times = io::read_file<io::exposure_t>(ARGS_input_file);
    for (io::exposure_t& entry : exposure_times) {
        if (FLAGS_bumblebee) {
            entry.exposure = absolute_value_bumblebee(entry.exposure & 0xFFF);
        } else if (FLAGS_ueye) {
            if (init) {
                std::swap(entry.exposure, last_exposure);
                last_exposure = absolute_value_ueye(last_exposure);
            } else {
               entry.exposure = absolute_value_ueye(entry.exposure);
               last_exposure = entry.exposure;
               init = true;
            }
        }

        entry.timestamp += FLAGS_cam_shift;
        if (FLAGS_compensate_exposure) entry.timestamp += entry.exposure / 2;
    }

    io::write_file(exposure_times, ARGS_input_file, EXPOSURE_HEADER);

    return 0;
}


#define PROGRAM_NAME \
    "copy"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(uint64, o, 0, "Sequence offset")                                                     \
    FLAG_CASE(uint64, n, 0, "Max sequence length")                                                 \
    FLAG_CASE(uint64, s, 0, "Skip sequence elements")

#define ARGS_CASES                                                                                 \
    ARG_CASE(input_path)                                                                           \
    ARG_CASE(output_path)

// STL
#include <cstddef>
#include <iostream>

// Boost
#include <boost/filesystem.hpp>

#include "args.hpp"
#include "io.hpp"
#include "sequence.hpp"

int main(int argc, char *argv[]) {

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    namespace fs = boost::filesystem;

    fs::path input_path = ARGS_input_path;
    fs::path output_path = ARGS_output_path;

    if (!fs::is_directory(input_path))
        std::cout << "Invalid input path" << std::endl;

    std::vector<std::string> sequence = getSequence<io::timestamp_t>(input_path.string(), FLAGS_o, FLAGS_n, FLAGS_s);

    fs::create_directories(output_path);
    std::size_t ctr = 0;
    for (const std::string &file : sequence) {
        std::cout << "\r" << ctr++ << " / " << sequence.size() << std::flush;
        fs::path file_path = file;
        fs::path new_file_path = output_path / file_path.filename();
        fs::copy_file(file_path, new_file_path, fs::copy_option::fail_if_exists);
    }
    std::cout << "\r" << ctr << " / " << sequence.size() << std::endl;

    return 0;
}


#define PROGRAM_NAME \
    "split"

// TODO Define so, ss, sn and eo, es, en (start and end subsequence parameters)
#define FLAGS_CASES                                                                                \
    FLAG_CASE(string, cameras, "", "Camera names, separeted by comas")                             \
    FLAG_CASE(uint64, so, 0, "Start segment sequence offset")                                      \
    FLAG_CASE(uint64, sn, 0, "Start segment sequence length")                                      \
    FLAG_CASE(uint64, ss, 0, "Start segment sequence skip")                                        \
    FLAG_CASE(uint64, eo, 0, "End segment sequence offset")                                        \
    FLAG_CASE(uint64, en, 0, "End segment sequence length")                                        \
    FLAG_CASE(uint64, es, 0, "End segment sequence skip")                                          \
    FLAG_CASE(string, output_path, "", "Output images path")

#define ARGS_CASES                                                                                 \
    ARG_CASE(input_path)

// STL
#include <cstddef>
#include <iostream>
#include <iterator>
#include <string>

// Boost
#include <boost/filesystem.hpp>

#include "args.hpp"
#include "io.hpp"
#include "macros.h"
#include "misc.h"
#include "sequence.hpp"

void ValidateArgs() {
    RUNTIME_ASSERT(boost::filesystem::is_directory(ARGS_input_path));
}

void ValidateFlags() {
    RUNTIME_ASSERT(!FLAGS_cameras.empty());

    boost::filesystem::path input_path = ARGS_input_path;
    std::vector<std::string> cameras = CSVToVector<std::string>(FLAGS_cameras);
    for (const std::string& camera_name : cameras) {
        RUNTIME_ASSERT(boost::filesystem::is_directory(input_path / (camera_name + "/data")));
        boost::filesystem::path images_path = input_path / (camera_name + "/data");
        std::vector<std::string> sequence = getSequence<io::timestamp_t>(images_path.string());
        RUNTIME_ASSERT(FLAGS_eo < sequence.size());
    }

    RUNTIME_ASSERT(FLAGS_so + FLAGS_sn*(FLAGS_ss+1) < FLAGS_eo);

    if (FLAGS_output_path.empty()) {
        FLAGS_output_path = boost::filesystem::current_path().string();
    } else {
        boost::filesystem::create_directories(FLAGS_output_path);
        RUNTIME_ASSERT(boost::filesystem::is_directory(FLAGS_output_path));
        RUNTIME_ASSERT(boost::filesystem::is_empty(FLAGS_output_path));
    }

    boost::filesystem::path output_path = FLAGS_output_path;
    for (const std::string& camera_name : cameras) {
        boost::filesystem::path camera_path = output_path / camera_name;
        boost::filesystem::create_directory(camera_path);
        RUNTIME_ASSERT(boost::filesystem::is_directory(camera_path));
        RUNTIME_ASSERT(boost::filesystem::is_empty(camera_path));
    }
}

int main(int argc, char *argv[]) {

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateArgs();
    ValidateFlags();

    namespace fs = boost::filesystem;

    boost::filesystem::path input_path = ARGS_input_path;
    boost::filesystem::path output_path = FLAGS_output_path;
    std::vector<std::string> camera_names = CSVToVector<std::string>(FLAGS_cameras);

    for (const std::string& camera_name : camera_names) {
        std::cout << "Processing " << camera_name << " ..." << std::endl;

        fs::path images_path = input_path / (camera_name + "/data");
        fs::path camera_path = output_path / camera_name;

        std::vector<std::string> sequence = getSequence<io::timestamp_t>(images_path.string());

        std::vector<std::string> start_seq = downsample(sequence, FLAGS_so, FLAGS_sn, FLAGS_ss);
        std::vector<std::string> end_seq = downsample(sequence, FLAGS_eo, FLAGS_en, FLAGS_es);

        std::size_t ctr = 0;
        std::size_t N = start_seq.size() + end_seq.size();

        for (const std::string& file : start_seq) {
            std::cout << "\r" << ctr++ << " / " << N << " " << std::flush;
            fs::path file_path = file;
            fs::path new_file_path = camera_path / (camera_name + "_" + file_path.filename().string());
            fs::copy_file(file_path, new_file_path, fs::copy_option::fail_if_exists);
        }

        for (const std::string& file : end_seq) {
            std::cout << "\r" << ctr++ << " / " << N << " " << std::flush;
            fs::path file_path = file;
            fs::path new_file_path = camera_path / (camera_name + "_" + file_path.filename().string());
            fs::copy_file(file_path, new_file_path, fs::copy_option::fail_if_exists);
        }

        std::cout << "\r" << ctr << " / " << N << std::endl;
    }

    return 0;
}

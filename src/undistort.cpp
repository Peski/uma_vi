
#define PROGRAM_NAME \
    "undistort"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(string, model_in, "NULL", "Input distortion model")                                  \
    FLAG_CASE(string, model_out, "NULL", "Output distortion model")                                \
    FLAG_CASE(string, size_in, "NULL", "Input image size")                                         \
    FLAG_CASE(string, size_out, "NULL", "Output image size")                                       \
    FLAG_CASE(string, params_in, "NULL", "Input image distortion parameters")                      \
    FLAG_CASE(string, params_out, "NULL", "Output image distortion parameters")                    

#define ARGS_CASES                                                                                 \

// STL
#include <iostream>
#include <stdexcept>
#include <string>

// Boost
#include <boost/filesystem.hpp>

// GFlags
#include <gflags/gflags.h>

// OpenCV
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "args.hpp"
#include "macros.h"

#include "colmap/misc.h"
#include "colmap/camera_models.h"
#include "colmap/types.hpp"

// TODO Add output_dir flag?

void ValidateFlags() {
  RUNTIME_ASSERT(colmap::ExistsCameraModelWithName(FLAGS_model_in));
  RUNTIME_ASSERT(colmap::ExistsCameraModelWithName(FLAGS_model_out));
}

template<typename T>
bool CheckSize(const std::vector<T>& v) {
  if (v.size() != 2) return false;

  for (const T& e : v)
    if (e < T(0)) return false;

  return true;
}

// Inspired by:
// https://github.com/ethz-asl/image_undistort
int main(int argc, char* argv[]) {

  // Handle help flag
  if (args::HelpRequired(argc, argv)) {
    args::ShowHelp();
    return 0;
  }
    
  // Parse input flags
  args::ParseCommandLineNonHelpFlags(&argc, &argv, true);
  
  ValidateFlags();

  std::vector<int> size_in = colmap::CSVToVector<int>(FLAGS_size_in);
  if (!CheckSize<int>(size_in)) {
    std::cout << "Invalid input image size" << std::endl;
    return -1;
  }

  std::vector<int> size_out = colmap::CSVToVector<int>(FLAGS_size_out);
  if (!CheckSize<int>(size_out)) {
    std::cout << "Invalid output image size" << std::endl;
    return -1;
  }    

  std::vector<double> params_in = colmap::CSVToVector<double>(FLAGS_params_in);
  colmap::Camera camera_in;
  camera_in.SetModelIdFromName(FLAGS_model_in);
  camera_in.SetParams(params_in);
  if (!camera_in.VerifyParams()) {
    std::cout << "Invalid input image distorition parameters" << std::endl;
    std::cout << "(for " << FLAGS_model_in << " they are: " << camera_in.ParamsInfo() << ")" << std::endl;
    return -1;
  }

  camera_in.SetWidth(size_in.at(0));
  camera_in.SetHeight(size_in.at(1));

  std::vector<double> params_out = colmap::CSVToVector<double>(FLAGS_params_out);
  colmap::Camera camera_out;
  camera_out.SetModelIdFromName(FLAGS_model_out);
  camera_out.SetParams(params_out);
  if (!camera_out.VerifyParams()) {
    std::cout << "Invalid output image distorition parameters" << std::endl;
    std::cout << "(for " << FLAGS_model_out << " they are: " << camera_out.ParamsInfo() << ")" << std::endl;
    return -1;
  }

  camera_out.SetWidth(size_out.at(0));
  camera_out.SetHeight(size_out.at(1));

  // Compute the remap maps
  cv::Mat map_x, map_y;
  cv::Size resolution_in(camera_in.Width(), camera_in.Height());
  cv::Size resolution_out(camera_out.Width(), camera_out.Height());

  // Initialize maps
  cv::Mat map_x_float(resolution_out, CV_32FC1);
  cv::Mat map_y_float(resolution_out, CV_32FC1);

//    bool empty_pixels = false;

  for (int v = 0; v < resolution_out.height; ++v) {
    for (int u = 0; u < resolution_out.width; ++u) {
      Eigen::Vector2d pixel_location(u, v);
      Eigen::Vector2d world_pixel_location;
      Eigen::Vector2d undistorted_pixel_location;

      world_pixel_location = camera_out.ImageToWorld(pixel_location);
      undistorted_pixel_location = camera_in.WorldToImage(world_pixel_location);

      // Insert in map
      map_x_float.at<float>(v, u) = static_cast<float>(undistorted_pixel_location.x());
      map_y_float.at<float>(v, u) = static_cast<float>(undistorted_pixel_location.y());

//      if ((undistorted_pixel_location.x() < 0) ||
//          (undistorted_pixel_location.y() < 0) ||
//          (undistorted_pixel_location.x() >= resolution_in.width) ||
//          (undistorted_pixel_location.y() >= resolution_in.height)) {
//            empty_pixels = true;
//      }
    }
  }

  // convert to fixed point maps for increased speed
  cv::convertMaps(map_x_float, map_y_float, map_x, map_y, CV_16SC2);

  int n = argc - 1;
  for (int i = 1; i < argc; ++i) {
    std::string filename = argv[i];
    std::cout << "\r" << i << " / " << n << std::flush;

    if (!boost::filesystem::is_regular_file(filename)) {
      std::cout << "\r[Warning] Ignoring input: " << filename << " (invalid file)" << std::endl;
      continue;
    }

    cv::Mat image = cv::imread(filename);

    if (image.empty()) {
      std::cout << "\r[Warning] Ignoring image: " << filename << " (invalid image)" << std::endl;
      continue;
    }

    if (image.size() != resolution_in) {
      std::cout << "\r[Warning] Ignoring image: " << filename << " (invalid image resolution)" << std::endl;
      continue;
    }

    cv::Mat image_transformed;
    cv::remap(image, image_transformed, map_x, map_y, cv::INTER_CUBIC, cv::BORDER_CONSTANT);
    cv::imwrite(filename, image_transformed);
  }
  std::cout << std::endl;

  return 0;
}
